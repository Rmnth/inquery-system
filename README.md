# CakePHP Application Skeleton
 
Proyecto creado con cakephp 3 
 
Se necesita tener instalado composer 
 
## Installation
 
Se deben correr los siguientes comandos en la carpeta del proyecto
 
- composer install
- composer update
 
## Creacion de la base de datos
 
- En la carpeta config se debe buscar el archivo app.php
- Dentro de esta se debe buscar la siguiente estructura
 
Datasources' => [
        'default' => [
            'username' => 'root',
            'password' => 'clavegenerica',
            'database' => 'inquery',
			
Aqui se debe reemplazar los campos 'username', 'password' y 'database' por las credenciales de su motor de base de datos personal,
en nuestro caso son las credenciales de phpmyadmin
 
Se debe crear la base de datos 'inquery', le puede dar el nombre que usted desee pero debe ser el mismo que en el campo 'database'
 
Una vez hecho esto y corridos los comandos ya mencionados se deben correr estos para poblar la base de datos
 
- bin/cake migrations migrate
- bin/cake migrations seed
 
El primer comando creara las tablas de la base de datos junto con las relaciones de estas
El segundo creara los usuarios de prueba del sistema, las credenciales de estos son:
 
Usuario 1:
- correo@prueba.cl
- s0p0rt3
 
Usuario 2:
- correo@prueba2.cl
- hola
 
 