<?php
use Migrations\AbstractMigration;

class Initial extends AbstractMigration
{
    public function up()
    {

        $this->table('alternatives', ['id' => false, 'primary_key' => ['ID']])
            ->addColumn('ID', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('text', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('iscorrect', 'integer', [
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('iditems', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'iditems',
                ]
            )
            ->create();

        $this->table('city', ['id' => false, 'primary_key' => ['ID']])
            ->addColumn('ID', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->create();

        $this->table('consulting', ['id' => false, 'primary_key' => ['ID']])
            ->addColumn('ID', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('direction', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('idprofile', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('idresearch', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'idprofile',
                ]
            )
            ->addIndex(
                [
                    'idresearch',
                ]
            )
            ->create();

        $this->table('header', ['id' => false, 'primary_key' => ['ID']])
            ->addColumn('ID', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('header', 'string', [
                'default' => null,
                'limit' => 500,
                'null' => true,
            ])
            ->create();

        $this->table('items', ['id' => false, 'primary_key' => ['ID']])
            ->addColumn('ID', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('question', 'string', [
                'default' => null,
                'limit' => 500,
                'null' => false,
            ])
            ->addColumn('idtest', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'idtest',
                ]
            )
            ->create();

        $this->table('profile', ['id' => false, 'primary_key' => ['ID']])
            ->addColumn('ID', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('mail', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('pass', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->create();

        $this->table('records', ['id' => false, 'primary_key' => ['ID']])
            ->addColumn('ID', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('idsample', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('idresearch', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('idalternative', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('response', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('isright', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addIndex(
                [
                    'idalternative',
                ]
            )
            ->addIndex(
                [
                    'idresearch',
                ]
            )
            ->addIndex(
                [
                    'idsample',
                ]
            )
            ->create();

        $this->table('region', ['id' => false, 'primary_key' => ['ID']])
            ->addColumn('ID', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->create();

        $this->table('reports', ['id' => false, 'primary_key' => ['ID']])
            ->addColumn('ID', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('idtest', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('idsample', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('diffindex', 'float', [
                'default' => null,
                'null' => false,
                'precision' => 2,
                'scale' => 2,
            ])
            ->addColumn('discindex', 'float', [
                'default' => null,
                'null' => false,
                'precision' => 2,
                'scale' => 2,
            ])
            ->addIndex(
                [
                    'idtest',
                ]
            )
            ->addIndex(
                [
                    'idtest',
                ]
            )
            ->addIndex(
                [
                    'idsample',
                ]
            )
            ->create();

        $this->table('researcher', ['id' => false, 'primary_key' => ['ID']])
            ->addColumn('ID', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('idprofile', 'integer', [
                'default' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'idprofile',
                ]
            )
            ->create();

        $this->table('sample', ['id' => false, 'primary_key' => ['ID']])
            ->addColumn('ID', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('idtest', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('idtarget', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('inidate', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('enddate', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'idtarget',
                ]
            )
            ->addIndex(
                [
                    'idtest',
                ]
            )
            ->create();

        $this->table('target', ['id' => false, 'primary_key' => ['ID']])
            ->addColumn('ID', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('mail', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('gender', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('birthdate', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('city', 'integer', [
                'default' => null,
                'limit' => 3,
                'null' => false,
            ])
            ->addColumn('region', 'integer', [
                'default' => null,
                'limit' => 3,
                'null' => false,
            ])
            ->addIndex(
                [
                    'city',
                ]
            )
            ->addIndex(
                [
                    'region',
                ]
            )
            ->create();

        $this->table('tests', ['id' => false, 'primary_key' => ['ID']])
            ->addColumn('ID', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('obs', 'string', [
                'default' => null,
                'limit' => 500,
                'null' => true,
            ])
            ->addColumn('idheader', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'idheader',
                ]
            )
            ->create();

        $this->table('alternatives')
            ->addForeignKey(
                'iditems',
                'items',
                'ID',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('consulting')
            ->addForeignKey(
                'idprofile',
                'profile',
                'ID',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'idresearch',
                'researcher',
                'ID',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('items')
            ->addForeignKey(
                'idtest',
                'tests',
                'ID',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('records')
            ->addForeignKey(
                'idalternative',
                'alternatives',
                'ID',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'idresearch',
                'researcher',
                'ID',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'idsample',
                'sample',
                'ID',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('reports')
            ->addForeignKey(
                'idtest',
                'sample',
                'ID',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'idtest',
                'tests',
                'ID',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('researcher')
            ->addForeignKey(
                'idprofile',
                'profile',
                'ID',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('sample')
            ->addForeignKey(
                'idtarget',
                'target',
                'ID',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'idtest',
                'tests',
                'ID',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('target')
            ->addForeignKey(
                'city',
                'city',
                'ID',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'region',
                'region',
                'ID',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('tests')
            ->addForeignKey(
                'idheader',
                'header',
                'ID',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();
    }

    public function down()
    {
        $this->table('alternatives')
            ->dropForeignKey(
                'iditems'
            )->save();

        $this->table('consulting')
            ->dropForeignKey(
                'idprofile'
            )
            ->dropForeignKey(
                'idresearch'
            )->save();

        $this->table('items')
            ->dropForeignKey(
                'idtest'
            )->save();

        $this->table('records')
            ->dropForeignKey(
                'idalternative'
            )
            ->dropForeignKey(
                'idresearch'
            )
            ->dropForeignKey(
                'idsample'
            )->save();

        $this->table('reports')
            ->dropForeignKey(
                'idtest'
            )
            ->dropForeignKey(
                'idtest'
            )->save();

        $this->table('researcher')
            ->dropForeignKey(
                'idprofile'
            )->save();

        $this->table('sample')
            ->dropForeignKey(
                'idtarget'
            )
            ->dropForeignKey(
                'idtest'
            )->save();

        $this->table('target')
            ->dropForeignKey(
                'city'
            )
            ->dropForeignKey(
                'region'
            )->save();

        $this->table('tests')
            ->dropForeignKey(
                'idheader'
            )->save();

        $this->table('alternatives')->drop()->save();
        $this->table('city')->drop()->save();
        $this->table('consulting')->drop()->save();
        $this->table('header')->drop()->save();
        $this->table('items')->drop()->save();
        $this->table('profile')->drop()->save();
        $this->table('records')->drop()->save();
        $this->table('region')->drop()->save();
        $this->table('reports')->drop()->save();
        $this->table('researcher')->drop()->save();
        $this->table('sample')->drop()->save();
        $this->table('target')->drop()->save();
        $this->table('tests')->drop()->save();
    }
}
