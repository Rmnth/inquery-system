<?php
use Migrations\AbstractSeed;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Roles seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'mail' => 'correo@prueba.cl',
                'pass' => (new DefaultPasswordHasher)->hash('s0p0rt3'),
            ],
            [
                'id' => '2',
                'mail' => 'correo@prueba2.cl',
                'pass' => (new DefaultPasswordHasher)->hash('hola'),
            ],
        ];

        $table = $this->table('profile');
        $table->insert($data)->save();
    }
}