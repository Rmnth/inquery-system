<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Alternatives Controller
 *
 * @property \App\Model\Table\AlternativesTable $Alternatives
 *
 * @method \App\Model\Entity\Alternative[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AlternativesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $alternatives = $this->paginate($this->Alternatives);

        $this->set(compact('alternatives'));
    }

    /**
     * View method
     *
     * @param string|null $id Alternative id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $alternative = $this->Alternatives->get($id, [
            'contain' => []
        ]);

        $this->set('alternative', $alternative);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $alternative = $this->Alternatives->newEntity();
        if ($this->request->is('post')) {
            $alternative = $this->Alternatives->patchEntity($alternative, $this->request->getData());
            if ($this->Alternatives->save($alternative)) {
                $this->Flash->success(__('The alternative has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The alternative could not be saved. Please, try again.'));
        }
        $this->set(compact('alternative'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Alternative id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $alternative = $this->Alternatives->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $alternative = $this->Alternatives->patchEntity($alternative, $this->request->getData());
            if ($this->Alternatives->save($alternative)) {
                $this->Flash->success(__('The alternative has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The alternative could not be saved. Please, try again.'));
        }
        $this->set(compact('alternative'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Alternative id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $alternative = $this->Alternatives->get($id);
        if ($this->Alternatives->delete($alternative)) {
            $this->Flash->success(__('The alternative has been deleted.'));
        } else {
            $this->Flash->error(__('The alternative could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
