<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Header Controller
 *
 * @property \App\Model\Table\HeaderTable $Header
 *
 * @method \App\Model\Entity\Header[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HeaderController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $header = $this->paginate($this->Header);

        $this->set(compact('header'));
    }

    /**
     * View method
     *
     * @param string|null $id Header id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $header = $this->Header->get($id, [
            'contain' => []
        ]);

        $this->set('header', $header);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $header = $this->Header->newEntity();
        if ($this->request->is('post')) {
            $header = $this->Header->patchEntity($header, $this->request->getData());
            if ($this->Header->save($header)) {
                $this->Flash->success(__('El encabezado a sido creado con exito.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El encabezado no a podido ser creado.'));
        }
        $this->set(compact('header'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Header id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $header = $this->Header->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $header = $this->Header->patchEntity($header, $this->request->getData());
            if ($this->Header->save($header)) {
                $this->Flash->success(__('El encabezado a sido editado con exito.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El encabezado no a podido ser editado'));
        }
        $this->set(compact('header'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Header id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $header = $this->Header->get($id);
        if ($this->Header->delete($header)) {
            $this->Flash->success(__('El encabezado a sido eliminado.'));
        } else {
            $this->Flash->error(__('El encabezado no a podido ser eliminado.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
