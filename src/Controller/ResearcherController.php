<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Researcher Controller
 *
 * @property \App\Model\Table\ResearcherTable $Researcher
 *
 * @method \App\Model\Entity\Researcher[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ResearcherController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $researcher = $this->paginate($this->Researcher);

        $this->set(compact('researcher'));
    }

    /**
     * View method
     *
     * @param string|null $id Researcher id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $researcher = $this->Researcher->get($id, [
            'contain' => []
        ]);

        $this->set('researcher', $researcher);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $researcher = $this->Researcher->newEntity();
        if ($this->request->is('post')) {
            $researcher = $this->Researcher->patchEntity($researcher, $this->request->getData());
            if ($this->Researcher->save($researcher)) {
                $this->Flash->success(__('The researcher has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The researcher could not be saved. Please, try again.'));
        }
        $this->set(compact('researcher'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Researcher id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $researcher = $this->Researcher->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $researcher = $this->Researcher->patchEntity($researcher, $this->request->getData());
            if ($this->Researcher->save($researcher)) {
                $this->Flash->success(__('The researcher has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The researcher could not be saved. Please, try again.'));
        }
        $this->set(compact('researcher'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Researcher id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $researcher = $this->Researcher->get($id);
        if ($this->Researcher->delete($researcher)) {
            $this->Flash->success(__('The researcher has been deleted.'));
        } else {
            $this->Flash->error(__('The researcher could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
