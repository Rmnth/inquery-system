<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tests Controller
 *
 * @property \App\Model\Table\TestsTable $Tests
 *
 * @method \App\Model\Entity\Test[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TestsController extends AppController
{
    public function initialize(){
        parent::initialize();

        $this->loadModel("Header");
        $this->loadModel("Tests");
        $this->loadModel("Items");
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $tests = $this->paginate($this->Tests->find('all'));
        $this->set(compact('tests'));
    }

    /**
     * View method
     *
     * @param string|null $id Test id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        
        $test = $this->Tests->get($id, [
            'contain' => []
        ]);

        $this->set('test', $test);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $test = $this->Tests->newEntity();
        $header = $this->Header->find('all');
        if ($this->request->is('post')) {
            $test = $this->Tests->patchEntity($test, $this->request->getData());
            try {
                if ($this->Tests->save($test)) {
                    $this->Flash->success(__('El test a sido guardado.'));
    
                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('El test no pudo ser guardado.'));
            } catch (\Exception $e) {
                $this->Flash->error(__('Alguno de los datos ingresados es erroneo.'));
            }
            
        }
        $this->set(compact(['test','header']));
    }
    /**
     * Edit method
     *
     * @param string|null $id Test id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function edit($id = null){

        $test = $this->Tests->get($id, [
            'contain' => [
                'Items'
            ]
        ]);
        $header = $this->Header->find('all');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $data = $this->request->getData();

            if(!isset($data["items"]))
                $data["items"] = [];

            $test = $this->tests->patchEntity($test, $data, [
                "associated" => [
                    "Items"
                ]
            ]);

            if ($this->Tests->save($test)) {
                $this->Flash->success(__('El test a sido guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El test no pudo ser guardado.'));
        }
        $this->set(compact(['test','header']));
    }

    /**
     * Delete method
     *
     * @param string|null $id Test id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $test = $this->Tests->get($id);
        if ($this->Tests->delete($test)) {
            $this->Flash->success(__('El test a sido eliminado.'));
        } else {
            $this->Flash->error(__('El test no pudo ser eliminado.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getInput(){
        if(!$this->request->is(["post", "ajax"]))
            return false;

        $this->layout = false;

        $data = $this->request->getData();

        switch($data["type_id"]){
            case 1:
                $name = "Texto";
                break;
            case 2:
                $name = "Selector";
                break;
            case 3:
                $name = "Radio";
                break;
            case 4:
                $name = "Area de texto";
                break;
            case 5:
                $name = "Checkbox";
                break;
            case 6:
                $name = "Número";
                break;
            case 7:
                $name = "Email";
                break;
            case 8:
                $name = "Télefono";
                break;
        }

        $this->set(compact("data", "name"));
    }
}
