<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Alternative Entity
 *
 * @property int $ID
 * @property string $text
 * @property int $iscorrect
 * @property int $iditems
 */
class Alternative extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'text' => true,
        'iscorrect' => true,
        'iditems' => true
    ];
}
