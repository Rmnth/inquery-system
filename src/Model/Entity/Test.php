<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Test Entity
 *
 * @property int $ID
 * @property string $name
 * @property string|null $obs
 * @property int $idheader
 */
class Test extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */

    public function _getTypes(){
        $types = [
            1 => "Texto",
            2 => "Selector",
            3 => "Radio",
            4 => "Area de texto",
            5 => "Checkbox",
            6 => "Número",
            7 => "Email",
            8 => "Teléfono"
        ];

        return $types;
    }

    public function _getActiveText(){
        return $this->active ? "Sí" : "No";
    }

    protected $_accessible = [
        'name' => true,
        'obs' => true,
        'idheader' => true
    ];
}
