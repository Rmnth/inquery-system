<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Alternatives Model
 *
 * @method \App\Model\Entity\Alternative get($primaryKey, $options = [])
 * @method \App\Model\Entity\Alternative newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Alternative[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Alternative|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Alternative saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Alternative patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Alternative[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Alternative findOrCreate($search, callable $callback = null, $options = [])
 */
class AlternativesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('alternatives');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmptyString('ID', 'create');

        $validator
            ->scalar('text')
            ->maxLength('text', 100)
            ->requirePresence('text', 'create')
            ->allowEmptyString('text', false);

        $validator
            ->integer('iscorrect')
            ->requirePresence('iscorrect', 'create')
            ->allowEmptyString('iscorrect', false);

        $validator
            ->integer('iditems')
            ->requirePresence('iditems', 'create')
            ->allowEmptyString('iditems', false);

        return $validator;
    }
}
