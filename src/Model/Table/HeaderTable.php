<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Header Model
 *
 * @method \App\Model\Entity\Header get($primaryKey, $options = [])
 * @method \App\Model\Entity\Header newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Header[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Header|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Header saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Header patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Header[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Header findOrCreate($search, callable $callback = null, $options = [])
 */
class HeaderTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('header');
        $this->setDisplayField('ID');
        $this->setPrimaryKey('ID');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmptyString('ID', 'create');

        $validator
            ->scalar('header')
            ->maxLength('header', 500)
            ->allowEmptyString('header');

        return $validator;
    }

    public function buildRules(RulesChecker $rules){
        $rules->add($rules->isUnique(['header'], 'Este encabezado ya se encuentra en uso'));
        return $rules;
    }
}
