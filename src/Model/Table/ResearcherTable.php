<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Researcher Model
 *
 * @method \App\Model\Entity\Researcher get($primaryKey, $options = [])
 * @method \App\Model\Entity\Researcher newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Researcher[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Researcher|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Researcher saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Researcher patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Researcher[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Researcher findOrCreate($search, callable $callback = null, $options = [])
 */
class ResearcherTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('researcher');
        $this->setDisplayField('name');
        $this->setPrimaryKey('ID');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('ID')
            ->allowEmptyString('ID', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->integer('idprofile')
            ->requirePresence('idprofile', 'create')
            ->allowEmptyFile('idprofile', false);

        return $validator;
    }
}
