<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Alternative $alternative
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Alternatives'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="alternatives form large-9 medium-8 columns content">
    <?= $this->Form->create($alternative) ?>
    <fieldset>
        <legend><?= __('Add Alternative') ?></legend>
        <?php
            echo $this->Form->control('text');
            echo $this->Form->control('iscorrect');
            echo $this->Form->control('iditems');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
