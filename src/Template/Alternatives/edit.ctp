<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Alternative $alternative
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $alternative->ID],
                ['confirm' => __('Are you sure you want to delete # {0}?', $alternative->ID)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Alternatives'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="alternatives form large-9 medium-8 columns content">
    <?= $this->Form->create($alternative) ?>
    <fieldset>
        <legend><?= __('Edit Alternative') ?></legend>
        <?php
            echo $this->Form->control('text');
            echo $this->Form->control('iscorrect');
            echo $this->Form->control('iditems');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
