<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Alternative[]|\Cake\Collection\CollectionInterface $alternatives
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Alternative'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="alternatives index large-9 medium-8 columns content">
    <h3><?= __('Alternatives') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('text') ?></th>
                <th scope="col"><?= $this->Paginator->sort('iscorrect') ?></th>
                <th scope="col"><?= $this->Paginator->sort('iditems') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($alternatives as $alternative): ?>
            <tr>
                <td><?= $this->Number->format($alternative->ID) ?></td>
                <td><?= h($alternative->text) ?></td>
                <td><?= $this->Number->format($alternative->iscorrect) ?></td>
                <td><?= $this->Number->format($alternative->iditems) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $alternative->ID]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $alternative->ID]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $alternative->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $alternative->ID)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
