<div class="content content-header">
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h1 class="page-title">Encabezados</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <?= $this->Html->link("Home",
                        ["controller" => "Pages", "action" => "dashboard"]
                    ); ?>
                </li>
                <li class="breadcrumb-item">
                    <?= $this->Html->link("Listado Encabezados",
                        ["controller" => "Header", "action" => "index"]
                    ); ?>
                </li>
                <li class="breadcrumb-item active">Editar</li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
        </div>
    </div>
</div>
<?= $this->Flash->render() ?>

<div class="content">
    <?= $this->Form->create($header) ?>
    <div class="row">
        <div class="col-12 col-md-9 mt-2">
            <div class="card">
                <div class="card-header bg-default">
                    <h5 class="card-title m-0">Datos del usuario </h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <?= $this->Form->control('header', ['class' => 'form-control', 'label' => 'Encabezado', 'placeholder' => 'Encabezado', 'type' => 'text']); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 mt-2">
            <div class="card">
            <div class="card-header bg-default">
                    <h5 class="card-title m-0">Acciones</h5>
                </div>
                <div class="card-body">
                    <?= $this->Form->button(__('<i class="far fa-save"></i> Guardar'),
                        [   
                            'type' => 'submit',
                            'class' => 'btn btn-outline-primary btn-block my-2', 
                            'escape' => false
                        ]);
                    ?>
                    <?= $this->Html->link(__('<i class="fas fa-clipboard-list"></i> Listado Encabezados'), 
                        ['action' => 'index'],
                        [
                            'class' => 'btn btn-outline-primary btn-block my-2', 
                            'escape' => false
                        ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>