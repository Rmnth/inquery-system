<tr class="text-left information-box-row" data-key="<?= $data["key"] ?>">
    <td class="align-middle">
        <?= $this->Form->control('form_fields.' . $data["key"] . '.name', [
            'class' => 'form-control',
            'label' => false,
            'required' => true,
            'placeholder' => 'Nombre del campo',
            'value' => $data["type_id"] == 9 ? "Selecciona tu espacio" : ""
        ]) ?>
    </td>
    <td class="align-middle text-center">
        <?= in_array($data["type_id"], [2, 3, 9]) ? $this->Form->control('form_fields.' . $data["key"] . '.options', [
            'class' => 'form-control',
            'label' => false,
            'required' => true,
            'placeholder' => 'Opción 1,Opción N',
            'value' => $data["type_id"] == 9 ? $centers : ""
        ]) : "N/A" ?>
    </td>
    <td class="align-middle">
        <div class="row d-flex center">
            <button type="button" class="btn btn-danger btn-flat delete">
                <i class="fa icon-special fa-minus"></i>
            </button>
        </div>
    </td>
</tr>