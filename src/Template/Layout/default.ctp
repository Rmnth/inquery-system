<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>InQuery</title>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

    <?= $this->Html->css('app.css') ?>
    <?= $this->Html->css('alert.css') ?>

    <!-- Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- Scripto -->
    <?= $this->Html->script(['app','jquery.min']) ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css">

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <header class="header">
        <div class="header-title">
            <span class="header-title-large">InQuery</span>
            <span class="header-title-min">InQ</span>
        </div>
        <div class="header-content">
            <div class="header-content-options">
                <a href="#" title="Opcion Generica 1"><i class="fas fa-cubes"></i></a>
                <a href="#" title="Opcion Generica 2"><i class="fas fa-cubes"></i></a>
                <a href="#" title="Opcion Generica 3"><i class="fas fa-cubes"></i></a>
            </div>
            <a href="#" class="header-content-user">
                <div class="header-content-user-img"><img src="assets/images/user.jpg" alt=""></div>
                <div class="header-content-user-name">Usuario Genérico</div>
            </a>
        </div>
    </header>

    <aside class="main-sidebar">
        <section class="sidebar">
            <form class="sidebar-search">
                <label for="">Buscar</label>
                <input type="text" class="form-control" placeholder="Buscar">
                <button class="btn btn-search"><i class="fa fa-search"></i></button>
            </form>
            <hr>
            <div class="sidebar-nav">
                <ul class="sidebar-nav-list">
                    <li class="sidebar-nav-item">
                        <?= $this->Html->link("<i class='fas fa-home'></i><span>Dashboard</span>",
                            ["controller" => "Pages", "action" => "dashboard"],
                            ["class" => "sidebar-nav-item-link", "escape" => false]
                        ); ?>
                    </li>
                    <li class="sidebar-nav-item">
                        <?= $this->Html->link("<i class='fas fa-users'></i><span>Perfiles</span>",
                            ["controller" => "Profile", "action" => "index"],
                            ["class" => "sidebar-nav-item-link", "escape" => false]
                        ); ?>
                    </li>
                    <li class="sidebar-nav-item">
                        <div class="dropdown"><i class="fab fa-elementor"></i><span>Modulo Encuestas</span></div>
                        <ul class="sidebar-nav-list">
                            <li class="sidebar-nav-item">
                                <?= $this->Html->link("<i class='fab fa-elementor'></i><span>Encabezado encuestas</span>",
                                    ["controller" => "Header", "action" => "index"],
                                    ["class" => "sidebar-nav-item-link", "escape" => false]
                                ); ?>
                            </li>
                            <li class="sidebar-nav-item">
                                <?= $this->Html->link("<i class='fab fa-elementor'></i><span>Esqueleto encuesta</span>",
                                    ["controller" => "Tests", "action" => "index"],
                                    ["class" => "sidebar-nav-item-link", "escape" => false]
                                ); ?>
                            </li>
                            <li class="sidebar-nav-item">
                                <?= $this->Html->link("<i class='fab fa-elementor'></i><span>Items encuestas</span>",
                                    ["controller" => "Items", "action" => "index"],
                                    ["class" => "sidebar-nav-item-link", "escape" => false]
                                ); ?>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-nav-item">
                        <?= $this->Html->link("<i class='fas fa-power-off'></i><span>Salir</span>",
                            ["controller" => "Pages", "action" => "logout"],
                            ["class" => "sidebar-nav-item-link", "escape" => false]
                        ); ?>
                    </li>
                    </li>
                </ul>
            </div>
        </section>
    </aside>

    <section class="content-wrapper">
        <?= $this->fetch('content') ?>
	</section>

        
    <footer>
    </footer>
</body>
</html>
