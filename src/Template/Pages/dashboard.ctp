<?= $this->Flash->render() ?>
<div class="content">
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="special-card">
						<h2>Formulario #0303456</h2>
						<div class="row">
							<div class="col-6 special-card-text">
								<p>Porcentaje de respuestas</p>
								<sub>Lorem ipsum dolor sit amet</sub>
							</div>
							<div class="col-6 special-card-graphic">
								<img src="assets/images/round.svg" alt="">
							</div>
						</div>
						<div class="special-card-link">
							<a href="#"> <i class="fas fa-external-link-alt"></i> Ver detalles </a>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="special-card">
						<h2>Formulario #0303456</h2>
						<div class="row">
							<div class="col-6 special-card-text">
								<p>Porcentaje de respuestas</p>
								<sub>Lorem ipsum dolor sit amet</sub>
							</div>
							<div class="col-6 special-card-graphic">
								<img src="assets/images/round.svg" alt="">
							</div>
						</div>
						<div class="special-card-link">
							<a href="#"> <i class="fas fa-external-link-alt"></i> Ver detalles </a>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="special-card">
						<h2>Formulario #0303456</h2>
						<div class="row">
							<div class="col-6 special-card-text">
								<p>Porcentaje de respuestas</p>
								<sub>Lorem ipsum dolor sit amet</sub>
							</div>
							<div class="col-6 special-card-graphic">
								<img src="assets/images/round.svg" alt="">
							</div>
						</div>
						<div class="special-card-link">
							<a href="#"> <i class="fas fa-external-link-alt"></i> Ver detalles </a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-6">
					<div class="card">
						<div class="card-header bg-default">
							<h5 class="card-title m-0">Grafico de lineas </h5>
						</div>
						<div class="card-body">
							<div class="chart-container" style="width:100%;height:300px;">
								<canvas id="myChart" width="200" height="200"></canvas>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6">
					<div class="card">
						<div class="card-header bg-default">
							<h5 class="card-title m-0">Grafico de barras </h5>
						</div>
						<div class="card-body">
							<div class="chart-container" style="widht:100%;height:300px;">
								<canvas id="bar-chart"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
</div>
		
	
	
	<script>
		new Chart(document.getElementById("myChart"),
			{
				type: 'line',
				data: {
				labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo'],
				datasets: [{ 
					data: [86,114,106,92,58],
					label: "Encuesta 1",
					borderColor: "#333333",
					backgroundColor: "#333333",
					fill: false
					},{ 
					data: [50,30,28,52,37],
					label: "Encuesta 2",
					borderColor: "#ef6594",
					backgroundColor: "#ef6594",
					fill: false
					}
				]
				},
				options: {
				title: {
					display: true,
					text: 'Metrica Genérica de Encuestas',
				},
				responsive: true, 
				maintainAspectRatio: false,
				}
			}
		);
		new Chart(document.getElementById("bar-chart"), {
        type: 'bar',
        data: {
          labels: ['1','2','3','4','5','6'],
          datasets: [{ 
              data: [30,50,9,53,63,22],
              label: "Horometro Diario",
              borderColor: "#0f80ce",
              backgroundColor: "#0f80ce",
              fill: false
            }
          ]
        },
        options: {
          title: {
            display: true,
            text: 'Horas de funcionamiento Diario'
          },
          responsive: true, 
          maintainAspectRatio: false,
        }
      });
	</script>