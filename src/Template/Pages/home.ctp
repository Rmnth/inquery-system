<?= $this->layout = false ?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>InQuery</title>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <?= $this->Html->css('app.css') ?>
</head>
<body>
      <section class="login">
      <?= $this->Flash->render('auth') ?>
         <div class="login-container">
            <div class="login-form">
               <div class="login-logo">
                  <?php echo $this->Html->image('logo.png', ['alt' => 'logo']); ?>
               </div>
               <h1>Ingresa Aquí</h1>
               <?=  $this->Form->create() ?>
                  <div class="form-group">
                     <label for="Usuario">Nombre Usuario</label>
                     <?= $this->form->input('mail',['id' => 'mail', 'class' => 'form-control', 'placeholder' => 'Email del usuario', 'label' => false, 'required']) ?>
                  </div>
                  <div class="form-group">
                     <label for="Usuario">Contraseña</label>
                     <?= $this->form->input('pass',['type' => 'password','id' => 'password', 'class' => 'form-control', 'placeholder' => 'Password', 'label' => false, 'required']) ?>
                  </div>
                  <div class="form-group">
                     <?= $this->Form->button('Acceder',['class' => 'btn btn-login', 'id' => 'sumbit','type' => 'submit', ]) ?>
                  </div>
                  <div class="form-group">
                     <a href="#">¿Olvidaste tu contraseña?</a><br>
                     <a href="#">¿No tienes una cuenta?</a>
                  </div>
                  <?=  $this->Form->end() ?>
            </div>
         </div>
      </section>
      <!-- Font awesome -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
      <!-- Scripto -->
      <?php echo $this->Html->script('app') ?>
   </body>
</html>