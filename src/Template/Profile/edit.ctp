<div class="content content-header">
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h1 class="page-title">Usuarios</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <?= $this->Html->link("Home",
                        ["controller" => "Pages", "action" => "dashboard"]
                    ); ?>
                </li>
                <li class="breadcrumb-item">
                    <?= $this->Html->link("Listado",
                        ["controller" => "Profile", "action" => "index"]
                    ); ?>
                </li>
                <li class="breadcrumb-item active">Editar</li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
        </div>
    </div>
</div>
<?= $this->Flash->render() ?>
<div class="content">
<?= $this->Form->create($profile) ?>
<div class="row">
    <div class="col-12 col-md-9 mt-2">
        <div class="card">
            <div class="card-header bg-default">
                <h5 class="card-title m-0">Datos del usuario </h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <?= $this->Form->control('mail', ['class' => 'form-control', 'label' => 'Mail', 'placeholder' => 'Mail', 'type' => 'email']); ?>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <?= $this->Form->control('pass', ['class' => 'form-control', 'label' => 'Password', 'placeholder' => 'Password', 'type' => 'password']); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-3 mt-2">
        <div class="card">
        <div class="card-header bg-default">
                <h5 class="card-title m-0">Acciones</h5>
            </div>
            <div class="card-body">
                <?= $this->Form->button(__('<i class="fas fa-plus"></i> Editar'),
                    [   
                        'type' => 'submit',
                        'class' => 'btn btn-outline-primary btn-block my-2', 
                        'escape' => false
                    ]);
                ?>
                <?= $this->Html->link(__('<i class="fas fa-clipboard-list"></i> Listado Perfiles'), 
                    ['action' => 'index'],
                    [
                        'class' => 'btn btn-outline-primary btn-block my-2', 
                        'escape' => false
                    ]);
                ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>
</div>