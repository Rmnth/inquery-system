<div class="content content-header">
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h1 class="page-title">Usuarios</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <?= $this->Html->link("Home",
                        ["controller" => "Pages", "action" => "dashboard"]
                    ); ?>
                </li>
                <li class="breadcrumb-item active">Listado</li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
        </div>
    </div>
</div>
<?= $this->Flash->render() ?>
<div class="content">
<div class="row">
    <div class="col-12 col-md-9 mt-2">
        <div class="card">
            <div class="card-header bg-default">
                <h5 class="card-title m-0">Usuarios del Sistema </h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('mail') ?></th>
                            <th scope="col" class="actions"><?= __('Actions') ?></th>
                        </thead>
                        <tbody>
                            <?php foreach ($profile as $profile): ?>
                            <tr>
                                <td><?= $this->Number->format($profile->ID) ?></td>
                                <td><?= h($profile->mail) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $profile->ID]) ?>
                                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $profile->ID]) ?>
                                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $profile->ID], ['confirm' => __('Seguro que quieres eliminar este usuario?', $profile->ID)]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php /*<div class="card-footer">
                <div class="paginator">
                    <ul class="pagination">
                        <?= $this->Paginator->first('<< ' . __('')) ?>
                        <?= $this->Paginator->prev('< ' . __('')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('') . ' >') ?>
                        <?= $this->Paginator->last(__('') . ' >>') ?>
                    </ul>
                    <p><?= $this->Paginator->counter(['format' => __('Pagina {{page}} de {{pages}}, mostrando {{current}} registro(s) de {{count}} totales')]) ?></p>
                </div>
            </div> */ ?>
        </div>
    </div>
    <div class="col-12 col-md-3 mt-2">
        <div class="card">
        <div class="card-header bg-default">
                <h5 class="card-title m-0">Gestión de Investigadores</h5>
            </div>
            <div class="card-body">
                <?= 
                    $this->Html->link(__('<i class="fas fa-plus"></i> Agregar Investigadores'), 
                    ['action' => 'add'],
                    [
                        'class' => 'btn btn-outline-primary btn-block my-2', 
                        'escape' => false
                    ]) ?>
            </div>
        </div>
    </div>
</div>
</div>