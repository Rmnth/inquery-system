<div class="content content-header">
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h1 class="page-title">Usuarios</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <?= $this->Html->link("Home",
                        ["controller" => "Pages", "action" => "dashboard"]
                    ); ?>
                </li>
                <li class="breadcrumb-item">
                    <?= $this->Html->link("Listado",
                        ["controller" => "Tests", "action" => "index"]
                    ); ?>
                </li>
                <li class="breadcrumb-item active">Ver</li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
        </div>
    </div>
</div>
<?= $this->Flash->render() ?>
<div class="content">
<div class="row"> 
    <div class="col-12 col-md-9 mt-2">
        <div class="card">
            <div class="card-header bg-default">
                <h5 class="card-title m-0">Datos del usuario </h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="form-group">
                            <label><?= __('Mail') ?></label>
                            <br>
                            <label><?= h($profile->mail) ?></label>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group">
                            <label><?= __('Pass') ?></label>
                            <br>
                            <label><?= h($profile->pass) ?></label>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group">
                            <label><?= __('ID') ?></label>
                            <br>
                            <label><?= h($profile->ID) ?></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-3 mt-2">
        <div class="card">
        <div class="card-header bg-default">
                <h5 class="card-title m-0">Acciones</h5>
            </div>
            <div class="card-body">
                <?= $this->Html->link(__('<i class="fas fa-pencil-alt"></i> Editar Perfil'), 
                    [
                        'action' => 'edit',
                        $profile->ID
                    ],
                    [
                        'class' => 'btn btn-outline-primary btn-block my-2', 
                        'escape' => false
                    ]) 
                ?>

                <?= $this->Html->link(__('<i class="fas fa-times"></i> Eliminar Perfil'), 
                    [
                        'action' => 'delete',
                        $profile->ID
                    ],
                    [
                        'class' => 'btn btn-outline-primary btn-block my-2', 
                        'escape' => false
                    ]) 
                ?>

                <?= $this->Html->link(__('<i class="fas fa-clipboard-list"></i> Listado Perfiles'), 
                    ['action' => 'index'],
                    [
                        'class' => 'btn btn-outline-primary btn-block my-2', 
                        'escape' => false
                    ]) 
                ?>

                <?= $this->Html->link(__('<i class="fas fa-plus"></i> Nuevo Perfil'), 
                    ['action' => 'add'],
                    [
                        'class' => 'btn btn-outline-primary btn-block my-2', 
                        'escape' => false
                    ]) 
                ?>
            </div>
        </div>
    </div>
</div>
</diV>