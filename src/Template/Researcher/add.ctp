<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Researcher $researcher
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Researcher'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="researcher form large-9 medium-8 columns content">
    <?= $this->Form->create($researcher) ?>
    <fieldset>
        <legend><?= __('Add Researcher') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('idprofile');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
