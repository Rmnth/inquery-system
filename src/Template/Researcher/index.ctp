<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Researcher[]|\Cake\Collection\CollectionInterface $researcher
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Researcher'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="researcher index large-9 medium-8 columns content">
    <h3><?= __('Researcher') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('idprofile') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($researcher as $researcher): ?>
            <tr>
                <td><?= $this->Number->format($researcher->ID) ?></td>
                <td><?= h($researcher->name) ?></td>
                <td><?= $this->Number->format($researcher->idprofile) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $researcher->ID]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $researcher->ID]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $researcher->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $researcher->ID)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
