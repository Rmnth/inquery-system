<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Researcher $researcher
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Researcher'), ['action' => 'edit', $researcher->ID]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Researcher'), ['action' => 'delete', $researcher->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $researcher->ID)]) ?> </li>
        <li><?= $this->Html->link(__('List Researcher'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Researcher'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="researcher view large-9 medium-8 columns content">
    <h3><?= h($researcher->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($researcher->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($researcher->ID) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Idprofile') ?></th>
            <td><?= $this->Number->format($researcher->idprofile) ?></td>
        </tr>
    </table>
</div>
