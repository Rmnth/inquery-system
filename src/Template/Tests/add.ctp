<div class="content content-header">
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h1 class="page-title">Test</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <?= $this->Html->link("Home",
                        ["controller" => "Pages", "action" => "dashboard"]
                    ); ?>
                </li>
                <li class="breadcrumb-item">
                    <?= $this->Html->link("Listado Test",
                        ["controller" => "Tests", "action" => "index"]
                    ); ?>
                </li>
                <li class="breadcrumb-item active">Agregar</li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
        </div>
    </div>
</div>
<?= $this->Flash->render() ?>

<div class="content">
    <?= $this->Form->create($test) ?>
    <div class="row">
        <div class="col-12 col-md-9 mt-2">
            <div class="card">
                <div class="card-header bg-default">
                    <h5 class="card-title m-0">Datos del Test </h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <?= $this->Form->control('name', ['class' => 'form-control', 'label' => 'Nombre', 'placeholder' => 'Nombre', 'type' => 'text']); ?>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <?= $this->Form->control('obs', ['class' => 'form-control', 'label' => 'Observación', 'placeholder' => 'Observación', 'type' => 'text']); ?>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="name">Encabezado</label>
                                <select name="idheader" class="form-control">
                                    <?php foreach ($header as $header):?>
                                        <option value="<?= $header->ID ?>"><?= $header->header ?></option>
                                    <?php endforeach; ?>    
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="card">
                <div class="card-header bg-default mt-3">
                    <i class="fas fa-align-justify"></i> Campos
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-12 col-md-3">
                            <?= $this->Form->control('types', ['class' => 'form-control', 'label' => 'Agregar campo', 'options' => $test->types, 'empty' => 'Seleccione']); ?>
                            <button class="btn btn-primary mt-2" id="btn-type">Agregar</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th scope="col">Tipo</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Placeholder</th>
                                        <th scope="col">Máx. caracteres</th>
                                        <th scope="col">Opciones</th>
                                        <th scope="col">Obligatorio</th>
                                        <th scope="col">Prioridad &nbsp;<i id="icon-geocode" class="fas fa-info-circle" style="color: blue;" data-toggle="tooltip" title="Ingresa un número del 1 al 10 para definir la prioridad de los campos al mostrarse, donde 1 es muy importante y 10 menos imporante"></i></th>
                                        <th scope="col">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody id="form-fields-container">
                                    <? if(strtolower($this->request->getParam("action")) == "edit"): foreach($test->form_fields as $key => $item): ?>
                                        <tr class="text-left information-box-row" data-key="<?= $key ?>">
                                            <td class="align-middle">
                                                <? 
                                                    echo $item->input_type_name;
                                                    echo $this->Form->hidden('form_fields.' . $key . '.id');
                                                    echo $this->Form->hidden('form_fields.' . $key . '.type_id');
                                                ?>
                                            </td>
                                            <td class="align-middle">
                                                <?= $this->Form->control('form_fields.' . $key . '.name', [
                                                    'class' => 'form-control',
                                                    'label' => false,
                                                    'placeholder' => 'Nombre del campo'
                                                ]) ?>
                                            </td>
                                            <td class="align-middle text-center">
                                                <?= !in_array($item["type_id"], [2, 3, 5, 9]) ? $this->Form->control('form_fields.' . $key . '.placeholder', [
                                                    'class' => 'form-control',
                                                    'label' => false,
                                                    'placeholder' => 'Texto ejemplo'
                                                ]) : "N/A" ?>
                                            </td>
                                            <td class="align-middle text-center">
                                                <?= !in_array($item["type_id"], [2, 3, 5, 9]) ? $this->Form->control('form_fields.' . $key . '.max_characters_number', [
                                                    'type' => 'number',
                                                    'class' => 'form-control validate-no-negative-values',
                                                    'label' => false,
                                                    'placeholder' => 'Límite de caracteres',
                                                    'min' => 1
                                                ]) : "N/A" ?>
                                            </td>
                                            <td class="align-middle text-center">
                                                <?= in_array($item["type_id"], [2, 3, 9]) ? $this->Form->control('form_fields.' . $key . '.options', [
                                                    'class' => 'form-control',
                                                    'label' => false,
                                                    'value' => $item->options_text,
                                                    'placeholder' => 'Opción 1,Opción N'
                                                ]) : "N/A" ?>
                                            </td>
                                            <td class="align-middle">
                                                <?= $this->Form->control('form_fields.' . $key . '.required', [
                                                    'type' => 'checkbox',
                                                    'class' => 'form-control',
                                                    'label' => false
                                                ]) ?>
                                            </td>
                                            <td class="align-middle">
                                                <?= $this->Form->control('form_fields.' . $key . '.priority', [
                                                    'class' => 'form-control',
                                                    'label' => false,
                                                    'placeholder' => 'Prioridad',
                                                    'type' => 'number',
                                                    'min' => 1,
                                                    'max' => 10
                                                ]) ?>
                                            </td>
                                            <td class="align-middle">
                                                <div class="row d-flex center">
                                                    <button type="button" class="btn btn-danger btn-flat delete">
                                                        <i class="fa icon-special fa-minus"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    <? endforeach; endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <?= $this->Form->button("<i class='fa fa-dot-circle-o'></i> Guardar", ['type' => 'submit', 'class' => 'btn btn-sm btn-success', 'escape' => false]) ?>
                    <?= $this->Html->link('<i class="fa fa-ban"></i> Cancelar', ['action' => 'index'], ['class' => 'btn btn-sm btn-danger', 'escape' => false]) ?>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 mt-2">
            <div class="card">
            <div class="card-header bg-default">
                    <h5 class="card-title m-0">Acciones</h5>
                </div>
                <div class="card-body">
                    <?= $this->Form->button(__('<i class="fas fa-plus"></i> Agregar'),
                        [   
                            'type' => 'submit',
                            'class' => 'btn btn-outline-primary btn-block my-2', 
                            'escape' => false
                        ]);
                    ?>
                    <?= $this->Html->link(__('<i class="fas fa-clipboard-list"></i> Listado Test'), 
                        ['action' => 'index'],
                        [
                            'class' => 'btn btn-outline-primary btn-block my-2', 
                            'escape' => false
                        ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
    <script>
        $("#btn-type").click(function(e){
            e.preventDefault();

            type_id = $("#types").val();
            lastKey = $('tr').last().attr('data-key');

            if($("#types").val() == ""){
                alert("Debes elegir una opción");
                
                return false;
            }

            key = parseInt(lastKey) + parseInt(1);

            if(isNaN(key))
                key = 0;

            $.ajax({
                url: "/tests/getInput",
                data: {type_id: type_id, key: key},
                type: "post",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                }, 
                success: function(data){
                    $("#form-fields-container").append(data);
                }
            });
        });

        $("body").on("blur", ".validate-no-negative-values", function(e){
            if($(this).val() < 0){
                $(this).val(1);
            }else{
                if(Number($(this).val()) > Number($(this).attr('max'))){
                    $(this).val($(this).attr('max'));
                }
            }
        });

        $("body").on("click", ".delete", function(e){
            e.preventDefault();

            $(this).closest("tr").remove();
        });
    </script>
</div>