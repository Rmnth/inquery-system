<tr class="text-left information-box-row" data-key="<?= $data["key"] ?>">
    <td class="align-middle">
        <? 
            echo $name;
            echo $this->Form->hidden('form_fields.' . $data["key"] . '.type_id', ["value" => $data["type_id"]]);
        ?>
    </td>
    <td class="align-middle">
        <?= $this->Form->control('form_fields.' . $data["key"] . '.name', [
            'class' => 'form-control',
            'label' => false,
            'required' => true,
            'placeholder' => 'Nombre del campo'
        ]) ?>
    </td>
    <td class="align-middle text-center">
        <?= !in_array($data["type_id"], [2, 3, 5, 9]) ? $this->Form->control('form_fields.' . $data["key"] . '.placeholder', [
            'class' => 'form-control',
            'label' => false,
            'placeholder' => 'Texto ejemplo'
        ]) : "N/A" ?>
    </td>
    <td class="align-middle text-center">
        <?= !in_array($data["type_id"], [2, 3, 5, 9]) ? $this->Form->control('form_fields.' . $data["key"] . '.max_characters_number', [
            'type' => 'number',
            'class' => 'form-control validate-no-negative-values',
            'label' => false,
            'placeholder' => 'Límite de caracteres',
            'min' => 1
        ]) : "N/A" ?>
    </td>
    <td class="align-middle text-center">
        <?= in_array($data["type_id"], [2, 3, 9]) ? $this->Form->control('form_fields.' . $data["key"] . '.options', [
            'class' => 'form-control',
            'label' => false,
            'required' => true,
            'placeholder' => 'Opción 1,Opción N'
        ]) : "N/A" ?>
    </td>
    <td class="align-middle">
        <?= $this->Form->control('form_fields.' . $data["key"] . '.required', [
            'type' => 'checkbox',
            'class' => 'form-control',
            'label' => false
        ]) ?>
    </td>
    <td class="align-middle">
        <?= $this->Form->control('form_fields.' . $data["key"] . '.priority', [
            'class' => 'form-control',
            'label' => false,
            'placeholder' => 'Prioridad',
            'type' => 'number',
            'value' => 1,
            'min' => 1,
            'max' => 10
        ]) ?>
    </td>
    <td class="align-middle">
        <div class="row d-flex center">
            <button type="button" class="btn btn-danger btn-flat delete">
                <i class="fa icon-special fa-minus"></i>
            </button>
        </div>
    </td>
</tr>