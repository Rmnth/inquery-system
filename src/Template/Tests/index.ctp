<div class="content content-header">
    <div class="row page-titles">
        <div class="col-md-5 col-8 align-self-center">
            <h1 class="page-title">Encuestas</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <?= $this->Html->link("Home",
                        ["controller" => "Pages", "action" => "dashboard"]
                    ); ?>
                </li>
                <li class="breadcrumb-item active">Listado</li>
            </ol>
        </div>
        <div class="col-md-7 col-4 align-self-center">
        </div>
    </div>
</div>
<?= $this->Flash->render() ?>
<div class="content">
    <div class="row">
        <div class="col-12 col-md-9 mt-2">
            <div class="card">
                <div class="card-header bg-default">
                    <h5 class="card-title m-0">Encuestas del Sistema </h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <th scope="col"><?= $this->Paginator->sort('ID') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('name','Nombre') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('obs','Observación') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('idheader','Codigo Encabezado') ?></th>
                                <th scope="col" class="actions"><?= __('Actions') ?></th>
                            </thead>
                            <tbody>
                                <?php foreach ($tests as $test):?>
                                <tr>
                                    <td><?= $this->Number->format($test->ID) ?></td>
                                    <td><?= h($test->name) ?></td>
                                    <td><?= h($test->obs) ?></td>
                                    <td><?= $test->idheader ?></td>
                                    <td class="actions">
                                        <?= $this->Html->link(__('View'), ['action' => 'view', $test->ID]) ?>
                                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $test->ID]) ?>
                                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $test->ID], ['confirm' => __('Seguro que quieres eliminar este test?', $test->ID)]) ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 mt-2">
            <div class="card">
            <div class="card-header bg-default">
                    <h5 class="card-title m-0">Gestión de Encabezados</h5>
                </div>
                <div class="card-body">
                    <?= 
                        $this->Html->link(__('<i class="fas fa-plus"></i> Agregar Test'), 
                        ['action' => 'add'],
                        [
                            'class' => 'btn btn-outline-primary btn-block my-2', 
                            'escape' => false
                        ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>