<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HeaderTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HeaderTable Test Case
 */
class HeaderTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\HeaderTable
     */
    public $Header;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Header'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Header') ? [] : ['className' => HeaderTable::class];
        $this->Header = TableRegistry::getTableLocator()->get('Header', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Header);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
