<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ResearcherTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ResearcherTable Test Case
 */
class ResearcherTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ResearcherTable
     */
    public $Researcher;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Researcher'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Researcher') ? [] : ['className' => ResearcherTable::class];
        $this->Researcher = TableRegistry::getTableLocator()->get('Researcher', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Researcher);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
